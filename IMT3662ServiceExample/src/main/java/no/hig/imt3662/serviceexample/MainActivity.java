package no.hig.imt3662.serviceexample;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * This is a simple project, that demonstrates the use of
 * background service with two different mechanisms for notifying the Activity.
 *
 * If your activity is not expected to be running, you can broadcast new intent that
 * a broadcast receiver will catch and pass to your main activity
 *
 * If your activity is expected to be running, you can use ResultReceiver mechanism
 * for continuously passing data from the service to the running activity.
 *
 * 
 *
 * Created by Mariusz Nowostawski nowostawski@gmail.com 4/11/13.
 */
public class MainActivity extends ActionBarActivity {

    PlaceholderFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.fragment = new PlaceholderFragment();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
        // We create a TimingService intent and start it here:
        final TimeServiceResultReceiver resultReceiver = new TimeServiceResultReceiver(null);
        final Intent i = new Intent(this, TimingService.class);
        i.putExtra("receiver", resultReceiver);
        startService(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        TextView tvCounter;

        public PlaceholderFragment() {
        }

        public TextView getCounterView() {
            return this.tvCounter;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            this.tvCounter = (TextView) rootView.findViewById(R.id.tv_counter);
            return rootView;
        }
    }


    // The main class for receiving the updates
    // from the background service
    class TimeServiceResultReceiver extends ResultReceiver {

        public TimeServiceResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == 100) {
                runOnUiThread(new UpdateCounter("Counter: "+resultData.getInt("counter")));
            }
        }
    }


    // Simple runnable responsible for
    // updating the UI
    class UpdateCounter implements Runnable {
        String updateString;

        public UpdateCounter(String updateString) {
            this.updateString = updateString;
        }
        public void run() {
            if (fragment.getCounterView() != null) {
                fragment.getCounterView().setText(updateString);
            }
        }
    }
}

package no.hig.imt3662.serviceexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Simple BroadcastReceiver example.
 *
 * Created by Mariusz Nowostawski nowostawski@gmail.com 4/11/13.
 */
public class TimeUpdatesReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // So, we have got a broadcast from our service
        // Let us extract one of the passed parameters
        final int counter = intent.getIntExtra("counter", -1);
        // Now, let us pass the parameter to our main activity
        /*
        final Intent i = new Intent(context, MainActivity.class);
        i.putExtra("counter", counter);
        context.startActivity(i);
        */
        Log.d("no.hig.imt3662.TimeUpdateReceiver", "I got the update: " + counter);
    }
}

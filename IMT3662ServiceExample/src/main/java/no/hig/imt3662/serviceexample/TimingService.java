package no.hig.imt3662.serviceexample;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;

/**
 * This is a demo service.
 * The service job is to count time in the background, and every 2 seconds send a broadcast.
 * The broadcast is then caught by the BroadcastReceiver, and handled by passing the data
 * to the MainActivity.
 *
 *
 * Created by Mariusz Nowostawski nowostawski@gmail.com 4/11/13.
 */
public class TimingService extends IntentService {

    // Handle for the result receiver,
    // which will be notified about time updates
    ResultReceiver resultReceiver;

    /**
     * The constructor must be present, and it must call
     * super constructor. The String will be used as an identifier
     * of the worker thread that is started automatically.
     */
    public TimingService() {
        super("TimeUpdateService");
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns,
     * IntentService stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        this.resultReceiver = intent.getParcelableExtra("receiver");

        // USE IT WITH CARE
        // to keep the service running even if the system is running low on resources
        // you have to tell the service to run in the "foreground"
        // see below for the method implementation
        //
        //      startServiceInForeground();
        // ---------------------------------------------

        // Ok - the main work for our service is:
        // * sleep for 2sec and then
        // * notify everyone that the time has elapsed
        final int SLEEP_TIME = 2000;
        int counter = 1;
        // TODO implement properly the shutting down of the service
        while (true) {
            try {
                // OUR MAIN WORK
                Thread.sleep(SLEEP_TIME);

                // First model, where the activity is not running, and we broadcast
                // the event to wake up the activity
                final Intent timeIntent = new Intent();
                timeIntent.putExtra("counter", counter);
                timeIntent.setAction("no.hig.TimeUpdate");
                sendBroadcast(timeIntent);

                // Second model, where the activity is running (most likely in the foreground)
                // and we want the activity to update its UI based on events from the service
                final Bundle bundle = new Bundle();
                bundle.putInt("counter", counter);
                this.resultReceiver.send(100, bundle);

                // and we finally update the counter
                counter++;
            } catch (InterruptedException e) {
                // this should never happen
                e.printStackTrace();
            }
        }
    }


    /**
     * This is used when we want to force our service to run until WE DECIDE to stop it.
     * Normally Android may decide to stop/garbage collect services it thinks are not used
     * anymore by any of the activities. If we want to keep that from happening, we need to
     * set the service to run in the foreground.
     */
    private void startServiceInForeground() {
        // What we do here is:
        // 1. setup what Intent should be invoked when we select a notification
        final Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
        // 2. setup the notification (in the notification bar up on the top of the screen)
        final Notification note = new NotificationCompat.Builder(this)
                .setContentTitle("HiG TimeService: ")
                .setContentText("time service update")
                .setSmallIcon(android.R.drawable.ic_popup_sync)
                .setContentIntent(pi)
                .build();
        note.flags |= Notification.FLAG_NO_CLEAR; // the notification cannot be cleared
        // 3. run our service in the foreground, with the notification attached
        startForeground(NOTIFICATION_ID, note);
    }

    // Arbitrary notification Identifier
    private int NOTIFICATION_ID = 1984;

}
